#include "stdafx.h"
#include "commctrl.h"
#include "resource.h"
#include "BoxHelper.h"
#include "SendBox.h"
#include "UART.h"

static HANDLE m_hThread;
static HANDLE hCurrUART;
static UINT   nCurrBaud;
static FILE* fpSend;
static char* pzSendFile;
static DWORD dwFileSize;
static DWORD dwFileSend;
static DWORD dwSendTick;

//WPARAM 状态
//LPARAM 数据长度
#define WM_SENDFILE		WM_USER+101

static DWORD WINAPI SendFileThread(void* arg)
{
	size_t ret;
	HWND hWnd;
	DWORD dwSend;
	DWORD dwMTU;
	char biSendBuff[1024];
	hWnd = (HWND)arg;
	dwMTU = nCurrBaud / 150;
	dwMTU = dwMTU < 1024 ? dwMTU : 1024;
	fseek(fpSend,0,SEEK_SET);
	while(1)
	{
		ret = fread(biSendBuff,1,dwMTU,fpSend);
		dwSend=UART_Write(hCurrUART,biSendBuff,(DWORD)ret);
		dwFileSend +=dwSend;
		if(dwSend != ret)
		{
			SendMessage(hWnd,WM_SENDFILE,0,1);
			break;
		}
		else if(ret < dwMTU)
		{
			SendMessage(hWnd,WM_SENDFILE,0,0);
			break;
		}
	}
	ExitThread(1);
}

static void UpdateSendRate(HWND hWnd)
{
	char szText[64];
	DWORD dwTick = GetTickCount() - dwSendTick + 1;
	DWORD dwSendSpeed = 1000 * dwFileSend / dwTick;
	sprintf(szText,"%d/%d字节(%d字节/秒)",dwFileSend,dwFileSize,dwSendSpeed);
	SetDlgItemText(hWnd,IDC_TEXT_RATE,szText);
	SendDlgItemMessage(hWnd,IDC_PROG,PBM_SETPOS,(dwFileSend*100)/dwFileSize,0);
}

static INT_PTR WINAPI SendBoxProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	DWORD dwPID;
	switch(msg)
	{
	case WM_INITDIALOG:
		SendDlgItemMessage(hWnd,IDC_PROG,PBM_SETRANGE,0,MAKELPARAM(0,100));
		SendDlgItemMessage(hWnd,IDC_PROG,PBM_SETPOS,0,0);
		SetDlgItemText(hWnd,IDC_TEXT_FILE,pzSendFile);
		
		fpSend = fopen(pzSendFile,"rb");
		if(fpSend==NULL)
		{
			SetDlgItemText(hWnd,IDC_TEXT_STATE, "打开文件失败!");
			break;
		}
		fseek(fpSend,0,SEEK_END);
		dwFileSend=0;
		dwFileSize=ftell(fpSend);
		
		SetDlgItemText(hWnd,IDC_TEXT_STATE, "正在发送...");
		m_hThread = CreateThread(NULL,0,SendFileThread, hWnd, 0, &dwPID);
		SetTimer(hWnd, 1, 100, NULL);
		dwSendTick = GetTickCount();
		break;
	case WM_TIMER:
		UpdateSendRate(hWnd);
		break;
	case WM_SENDFILE:
		if(wParam==0)
		{
			KillTimer(hWnd, 1);
			UpdateSendRate(hWnd);
			switch(lParam)
			{
			case 0:
				SetDlgItemText(hWnd,IDC_TEXT_STATE, "发送完成!");
				break;
			case 1:
				SetDlgItemText(hWnd,IDC_TEXT_STATE, "发送失败!");
				break;
			}
		}
		break;
	case WM_COMMAND:
		switch(wParam)
		{
		case IDOK:
		case IDCANCEL:
			TerminateThread(m_hThread,0);
			CloseHandle(m_hThread);
			EndDialog(hWnd,0);
			break;
		}
		break;
	}
	return 0;
}

void PopupSendBox(HINSTANCE hInst, HWND hParent, HANDLE hUART, char* pzName, UINT nBaud)
{
	hCurrUART = hUART;
	pzSendFile= pzName;
	nCurrBaud = nBaud;
	DialogBox(hInst,MAKEINTRESOURCE(IDD_SENDFILE), hParent, SendBoxProc);
}
