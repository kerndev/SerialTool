#pragma once

/*
* ����
* dwBaudRate:	1200-921600
* biDataBits:	5,6,7,8
* biStopBits:	ONESTOPBIT          0
				ONE5STOPBITS        1 
				TWOSTOPBITS         2
* biParity:		NOPARITY            0
				ODDPARITY           1
				EVENPARITY          2
				MARKPARITY          3
				SPACEPARITY         4
*/
HANDLE UART_Open(LPTSTR pzName);
void   UART_Close(HANDLE hUART);
void   UART_Setup(HANDLE hUART, DWORD dwBaudRate, BYTE biDataBits, BYTE biStopBits, BYTE biParity);
DWORD  UART_Write(HANDLE hUART, void *pData, DWORD dwLen);
DWORD  UART_Read(HANDLE hUART, void *pData, DWORD dwLen);
