#include "stdafx.h"
#include "UART.h"

HANDLE UART_Open(LPTSTR pzName)
{
	HANDLE hUART;
	COMMTIMEOUTS timeout;
	char szPortName[32];
	sprintf(szPortName,"\\\\.\\%s",pzName);
	hUART = CreateFile(szPortName,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL);
	if((hUART==INVALID_HANDLE_VALUE)||(hUART==NULL))
	{
		return NULL;
	}
	
	GetCommTimeouts(hUART, &timeout);
	timeout.ReadIntervalTimeout = MAXDWORD;
	timeout.ReadTotalTimeoutMultiplier = 0;
	timeout.ReadTotalTimeoutConstant = 0;
	timeout.WriteTotalTimeoutConstant = 0;
	timeout.WriteTotalTimeoutMultiplier = 0;
	SetCommTimeouts(hUART,&timeout);

	SetupComm(hUART, 64*1024, 0);
	PurgeComm(hUART, PURGE_RXCLEAR | PURGE_TXCLEAR);
	
	return hUART;
}

void UART_Close(HANDLE hUART)
{
	CloseHandle(hUART);
}

void UART_Setup(HANDLE hUART, DWORD dwBaudRate, BYTE biDataBits, BYTE biStopBits, BYTE biParity)
{
	DCB dcb;
	GetCommState(hUART, &dcb);
	dcb.BaudRate = dwBaudRate;
	dcb.Parity   = biParity;
	dcb.ByteSize = biDataBits;
	dcb.StopBits = biStopBits;
	SetCommState(hUART, &dcb);
}

DWORD UART_Read(HANDLE hUART, void *pData, DWORD dwLen)
{
	BOOL  biRet;
	DWORD dwRead = 0;
	biRet = ReadFile(hUART,pData,dwLen,&dwRead,NULL);
	return biRet?dwRead:0;
}

DWORD UART_Write(HANDLE hUART, void *pData, DWORD dwLen)
{
	BOOL  biRet;
	DWORD dwWrite;
	biRet = WriteFile(hUART,pData,dwLen,&dwWrite,NULL);
	return biRet?dwWrite:0;
}
