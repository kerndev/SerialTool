#include "stdafx.h"
#include "resource.h"
#include "BoxHelper.h"
#include "SerialTool.h"
#include "UART.h"
#include "EnumUART.h"
#include "convert.h"
#include "AboutBox.h"
#include "FileBox.h"
#include "SendBox.h"
#include "CbtHook.h"

#define MAX_DATA_LEN    (64*1024)
#define MAX_TEXT_LEN    (MAX_DATA_LEN * 3)

//接收数据消息
//WPARAM:数据长度
//LPARAM:数据指针
#define MSG_RECVDATA    WM_USER+100

static HINSTANCE m_hInst;
static BOOL      m_bInit;
static RECT      m_MinRect;
static RECT      m_CurRect;
static HANDLE    m_hUART;
static HANDLE    m_hThread;
static HFONT     m_hFont;
static COLORREF  m_custColor[16];
static DWORD     m_textColor;
static DWORD     m_backColor;
static HBRUSH    m_backBrush;
static HWND      m_hwndSend;
static HWND      m_hwndRecv;
static BOOL      m_bStopRecv;
static BOOL      m_bAutoSend;
static BOOL      m_bHEXSend;
static BOOL      m_bHEXDisp;
static BOOL      m_bRecvFile;
static FILE*     m_fpRecvFile;
static DWORD     m_dwSendCount;
static DWORD     m_dwRecvCount;
static char      m_szRecvBuff[MAX_TEXT_LEN];
static char      m_szSendBuff[MAX_TEXT_LEN];
static BYTE      m_biSendBuff[MAX_DATA_LEN];
static BYTE      m_biRecvBuff[MAX_DATA_LEN];

static void FillBaudRate(HWND hWnd)
{
	ComboBox_AddString(hWnd, "1200");
	ComboBox_AddString(hWnd, "2400");
	ComboBox_AddString(hWnd, "4800");
	ComboBox_AddString(hWnd, "9600");
	ComboBox_AddString(hWnd, "14400");
	ComboBox_AddString(hWnd, "19200");
	ComboBox_AddString(hWnd, "38400");
	ComboBox_AddString(hWnd, "57600");
	ComboBox_AddString(hWnd, "115200");
	ComboBox_AddString(hWnd, "230400");
	ComboBox_AddString(hWnd, "460800");
	ComboBox_AddString(hWnd, "921600");
	ComboBox_SetCurSel(hWnd, 8);
}

static void FillSerialPort(HWND hWnd)
{
	int i;
	int count;
	ComboBox_ResetContent(hWnd);
	count=EnumUART();
	for(i=0;i<count;i++)
	{
		ComboBox_AddString(hWnd, GetPortName(i));
	}
	ComboBox_SetCurSel(hWnd, 0);
}

static void FillDataBits(HWND hWnd)
{
	ComboBox_AddString(hWnd, "5");
	ComboBox_AddString(hWnd, "6");
	ComboBox_AddString(hWnd, "7");
	ComboBox_AddString(hWnd, "8");
	ComboBox_SetCurSel(hWnd, 3);
}

static void FillStopBits(HWND hWnd)
{
	ComboBox_AddString(hWnd, "1");
	ComboBox_AddString(hWnd, "1.5");
	ComboBox_AddString(hWnd, "2");
	ComboBox_SetCurSel(hWnd, 0);
}

static void FillParityBit(HWND hWnd)
{
	ComboBox_AddString(hWnd, "None");
	ComboBox_AddString(hWnd, "Odd");
	ComboBox_AddString(hWnd, "Even");
	ComboBox_AddString(hWnd, "Mark");
	ComboBox_AddString(hWnd, "Space");
	ComboBox_SetCurSel(hWnd, 0);
}

static void AppendRecvData(HWND hWnd, char* pzText)
{
	HWND hItem;
	LONG lTextLen;
	hItem = GetDlgItem(hWnd,IDC_EDIT_RECV);
	SetWindowRedraw(hItem, 0);
	lTextLen = Edit_GetTextLength(hItem);
	Edit_SetSel(hItem, lTextLen, lTextLen);
	Edit_ReplaceSel(hItem, pzText);
	SendMessage(hItem, WM_VSCROLL, SB_BOTTOM, 0);
	SetWindowRedraw(hItem, 1);
}

static void ClearRecvData(HWND hWnd)
{
	SetDlgItemText(hWnd,IDC_EDIT_RECV,"");
}

static void UpdateDataCounter(HWND hWnd)
{
	char szText[32];
	sprintf(szText,"发送:%d",m_dwSendCount);
	SetDlgItemText(hWnd,IDC_TEXT_SENDCOUNT,szText);
	sprintf(szText,"接收:%d",m_dwRecvCount);
	SetDlgItemText(hWnd,IDC_TEXT_RECVCOUNT,szText);
}

static void UpdateDeviceName(HWND hWnd, char* pzDevName)
{
	char szText[MAX_PATH];
	sprintf(szText,"串口设备:%s", pzDevName?pzDevName:"");
	SetDlgItemText(hWnd,IDC_TEXT_DEVICE,szText);
}

static void UpdateRecvData(HWND hWnd, BYTE* pData, DWORD dwLen)
{
	m_dwRecvCount += dwLen;
	UpdateDataCounter(hWnd);

	if(m_fpRecvFile != NULL)
	{
		fwrite(pData,1,dwLen,m_fpRecvFile);
	}
	
	if(m_bStopRecv)
	{
		return;
	}

	if(m_bHEXDisp)
	{
		BINToHEX(m_szRecvBuff, pData, dwLen);	
	}
	else
	{
		BINToGBK(m_szRecvBuff, pData, dwLen);
	}
	AppendRecvData(hWnd, m_szRecvBuff);
}

static DWORD WINAPI UARTRecvThread(void* arg)
{
	DWORD dwLen;
	HWND hWnd;
	hWnd = (HWND)arg;
	while(1)
	{
		Sleep(10);
		dwLen = UART_Read(m_hUART, m_biRecvBuff, MAX_DATA_LEN);
		if(dwLen != 0)
		{
			SendMessage(hWnd,MSG_RECVDATA,dwLen,(LPARAM)m_biRecvBuff);
		}
	}
}

static BOOL SendDataToUART(HWND hWnd)
{
	DWORD dwLen;
	DWORD dwRet;

	dwLen = GetDlgItemText(hWnd,IDC_EDIT_SEND, m_szSendBuff, MAX_TEXT_LEN);
	if(m_bHEXSend)
	{
		dwLen=HEXToBIN(m_szSendBuff,m_biSendBuff);
		dwRet=UART_Write(m_hUART, m_biSendBuff, dwLen);
	}
	else
	{
		dwRet=UART_Write(m_hUART, m_szSendBuff, dwLen);
	}
	m_dwSendCount += dwRet;
	UpdateDataCounter(hWnd);

	return (dwRet==dwLen);
}

static void SetSerialConfig(HWND hWnd, HANDLE hUART)
{
	UINT nBaudrate;
	UINT nDataBits;
	UINT nParity;
	UINT nStopBits;
	nBaudrate = GetDlgItemInt(hWnd,IDC_LIST_BAUDRATE,NULL,0);
	nDataBits = GetDlgItemInt(hWnd,IDC_LIST_DATABITS,NULL,0);
	nStopBits = (UINT)SendDlgItemMessage(hWnd,IDC_LIST_STOPBITS,CB_GETCURSEL,0,0);
	nParity   = (UINT)SendDlgItemMessage(hWnd,IDC_LIST_PARITY,CB_GETCURSEL,0,0);
	UART_Setup(hUART, nBaudrate, nDataBits, nStopBits, nParity);
}

static void OnBnClickedOpen(HWND hWnd)
{
	DWORD dwPID;
	int  nIndex;
	if(m_hUART == NULL)
	{
		nIndex = (int)SendDlgItemMessage(hWnd,IDC_LIST_PORT,CB_GETCURSEL,0,0);
		if(nIndex < 0)
		{
			MessageBox(hWnd,"没有选择串口!","打开失败", MB_OK|MB_ICONERROR);
			return;
		}
		m_hUART = UART_Open(GetPortName(nIndex));
		if(m_hUART == NULL)
		{
			MessageBox(hWnd,"打开串口失败!","打开失败", MB_OK|MB_ICONERROR);
			return;
		}
		m_hThread = CreateThread(0,0,UARTRecvThread,hWnd,0,&dwPID);
		if(m_hThread == NULL)
		{
			UART_Close(m_hUART);
			MessageBox(hWnd,"启动接收服务失败!","打开失败", MB_OK|MB_ICONERROR);
			return;
		}
		SetSerialConfig(hWnd, m_hUART);
		UpdateDeviceName(hWnd,GetDeviceName(nIndex));
		SetDlgItemText(hWnd,IDC_BTN_OPEN,"关闭串口(&O)");
		EnableDlgItem(hWnd,IDC_BTN_REFRESH,0);
		EnableDlgItem(hWnd,IDC_LIST_PORT,0);
	}
	else
	{
		TerminateThread(m_hThread,0);
		CloseHandle(m_hThread);
		UART_Close(m_hUART);
		m_hUART = NULL;
		EnableDlgItem(hWnd,IDC_BTN_REFRESH,1);
		EnableDlgItem(hWnd,IDC_LIST_PORT,1);
		SetDlgItemText(hWnd,IDC_BTN_OPEN,"打开串口(&O)");
		UpdateDeviceName(hWnd,NULL);
	}
}

static void OnBnClickedSend(HWND hWnd)
{
	BOOL bRet;
	bRet=SendDataToUART(hWnd);
	if(!bRet)
	{
		MessageBox(hWnd,"无法写入数据!","发送失败",MB_OK);
	}
}

static void OnBnClickedClear(HWND hWnd)
{
	ClearRecvData(hWnd);
}

static void OnBnClickedPause(HWND hWnd)
{
	m_bStopRecv = IsDlgButtonChecked(hWnd, IDC_BTN_PAUSE);
}

static void OnBnClickedZero(HWND hWnd)
{
	m_dwSendCount = 0;
	m_dwRecvCount = 0;
	UpdateDataCounter(hWnd);
}

static void OnBnClickedHEXSend(HWND hWnd)
{
	m_bHEXSend = IsDlgButtonChecked(hWnd,IDC_BTN_SENDHEX);
}

static void OnBnClickedHEXDisplay(HWND hWnd)
{
	m_bHEXDisp = IsDlgButtonChecked(hWnd,IDC_BTN_RECVHEX);
}

static void OnBnClickedAutoSend(HWND hWnd)
{
	int nInterval;
	m_bAutoSend=IsDlgButtonChecked(hWnd,IDC_BTN_AUTOSEND);
	EnableDlgItem(hWnd,IDC_EDIT_INTERVAL,!m_bAutoSend);
	EnableDlgItem(hWnd,IDC_BTN_SEND,!m_bAutoSend);
	EnableDlgItem(hWnd,IDC_BTN_SENDFILE,!m_bAutoSend);
	if(m_bAutoSend)
	{
		nInterval = GetDlgItemInt(hWnd,IDC_EDIT_INTERVAL,NULL,0);
		SetTimer(hWnd,1,nInterval,NULL);
	}
	else
	{
		KillTimer(hWnd,1);
	}
}

static void OnBnClickedRefresh(HWND hWnd)
{
	FillSerialPort(GetDlgItem(hWnd,IDC_LIST_PORT));
}

static void OnBnClickedRecvFile(HWND hWnd)
{
	char szFileName[MAX_PATH];
	m_bRecvFile = IsDlgButtonChecked(hWnd,IDC_BTN_RECVFILE);
	if(m_bRecvFile)
	{
		if(SaveFileBox(hWnd,"写入文件","所有文件(*.*)", szFileName))
		{
			m_fpRecvFile = fopen(szFileName,"wb+");
			if(m_fpRecvFile == NULL)
			{
				MessageBox(hWnd,"无法创建文件!","写入失败",MB_OK|MB_ICONERROR);
				CheckDlgButton(hWnd,IDC_BTN_RECVFILE,0);
			}
		}
		else
		{
			CheckDlgButton(hWnd,IDC_BTN_RECVFILE,0);
		}
	}
	else
	{
		if(m_fpRecvFile != NULL)
		{
			fclose(m_fpRecvFile);
			m_fpRecvFile = NULL;
		}
	}
}

static void OnBnClickedSaveFile(HWND hWnd)
{
	int len;
	FILE *fp;
	CHAR *sp;
	CHAR szFileName[MAX_PATH];
	HWND item;
	item = GetDlgItem(hWnd, IDC_EDIT_RECV);
	if(SaveFileBox(hWnd,"保存文件","所有文件(*.*)", szFileName))
	{
		fp = fopen(szFileName,"wb+");
		if(fp == NULL)
		{
			MessageBox(hWnd,"无法创建指定文件!","保存失败",MB_OK|MB_ICONERROR);
			return;
		}
		len = GetWindowTextLengthA(item) + 1;
		sp = (char *)malloc(len);
		if(sp == NULL)
		{
			fclose(fp);
			MessageBox(hWnd,"计算机内存不足!","保存失败",MB_OK|MB_ICONERROR);
			return;
		}
		len = GetWindowTextA(item, sp, len);
		fwrite(sp, 1, len, fp);
		fclose(fp);
		free(sp);
	}
}

static void OnBnClickedSendFile(HWND hWnd)
{
	char szFileName[MAX_PATH];
	if(OpenFileBox(hWnd,"发送文件","所有文件(*.*)", szFileName))
	{
		UINT baud = GetDlgItemInt(hWnd,IDC_LIST_BAUDRATE,NULL,0);
		PopupSendBox(m_hInst,hWnd,m_hUART,szFileName,baud);
	}
}

static void OnBnClickedFont(HWND hWnd)
{
	CHOOSEFONT cf;
	LOGFONT lf;
	memset(&cf, 0, sizeof(cf));
	cf.lStructSize = sizeof (CHOOSEFONT);
	cf.hwndOwner = hWnd; 
	cf.lpLogFont = &lf;
	cf.Flags = CF_SCREENFONTS | CF_NOVERTFONTS;
	if(!ChooseFontA(&cf))
	{
		return;
	}
	if(m_hFont != NULL)
	{
		DeleteFont(m_hFont);
	}
	m_hFont = CreateFontIndirectA(&lf);
	SetWindowFont(GetDlgItem(hWnd,IDC_EDIT_RECV), m_hFont, 1);
	SetWindowFont(GetDlgItem(hWnd,IDC_EDIT_SEND), m_hFont, 1);
}

static void OnBnClickedTextColor(HWND hWnd)
{
	CHOOSECOLOR cc;
	memset(&cc, 0, sizeof(cc));
	cc.lStructSize = sizeof (CHOOSECOLOR);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = m_custColor;
	cc.Flags = CC_SOLIDCOLOR;
	if (ChooseColor(&cc) )
	{
		if(m_textColor != cc.rgbResult)
		{
			m_textColor = cc.rgbResult;
			InvalidateRect(m_hwndRecv, NULL, FALSE);
			InvalidateRect(m_hwndSend, NULL, FALSE);
		}
	}
}

static void OnBnClickedBackColor(HWND hWnd)
{
	CHOOSECOLOR cc;
	memset(&cc, 0, sizeof(cc));
	cc.lStructSize = sizeof (CHOOSECOLOR);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = m_custColor;
	cc.Flags = CC_SOLIDCOLOR;
	if (ChooseColor(&cc) )
	{
		if(m_backColor != cc.rgbResult)
		{
			m_backColor = cc.rgbResult;
			DeleteBrush(m_backBrush);
			m_backBrush = CreateSolidBrush(m_backColor);
			InvalidateRect(m_hwndRecv, NULL, TRUE);
			InvalidateRect(m_hwndSend, NULL, TRUE);
		}
	}
}

static void OnTimer(HWND hWnd, WPARAM wParam)
{
	BOOL bRet;
	bRet=SendDataToUART(hWnd);
	if(!bRet)
	{
		CheckDlgButton(hWnd,IDC_BTN_AUTOSEND,0);
		OnBnClickedAutoSend(hWnd);
		MessageBox(hWnd,"无法发送数据,操作已取消!","自动发送失败",MB_OK);
	}
}

//下拉修改
static void OnDropdownChanged(HWND hWnd, UINT id)
{
	switch(id)
	{
	case IDC_LIST_BAUDRATE:
	case IDC_LIST_DATABITS:
	case IDC_LIST_PARITY:
	case IDC_LIST_STOPBITS:
		if(m_hUART != NULL)
		{
			SetSerialConfig(hWnd, m_hUART);
		}
		break;
	}
}

//按键事件
static void OnBnClicked(HWND hWnd, UINT id)
{
	switch(id)
	{
	case IDCANCEL:
		EndDialog(hWnd,0);
		break;
	case IDC_BTN_OPEN:
		OnBnClickedOpen(hWnd);
		break;
	case IDC_BTN_SEND:
		OnBnClickedSend(hWnd);
		break;
	case IDC_BTN_ZERO:
		OnBnClickedZero(hWnd);
		break;
	case IDC_BTN_CLEAR:
		OnBnClickedClear(hWnd);
		break;
	case IDC_BTN_PAUSE:
		OnBnClickedPause(hWnd);
		break;
	case IDC_BTN_RECVHEX:
		OnBnClickedHEXDisplay(hWnd);
		break;
	case IDC_BTN_AUTOSEND:
		OnBnClickedAutoSend(hWnd);
		break;
	case IDC_BTN_SENDHEX:
		OnBnClickedHEXSend(hWnd);
		break;
	case IDC_BTN_REFRESH:
		OnBnClickedRefresh(hWnd);
		break;
	case IDC_BTN_RECVFILE:
		OnBnClickedRecvFile(hWnd);
		break;
	case IDC_BTN_SENDFILE:
		OnBnClickedSendFile(hWnd);
		break;
	case IDC_BTN_SAVE:
		OnBnClickedSaveFile(hWnd);
		break;
	case IDC_BTN_FONT:
		OnBnClickedFont(hWnd);
		break;
	case IDC_BTN_TEXTCOLOR:
		OnBnClickedTextColor(hWnd);
		break;
	case IDC_BTN_BKCOLOR:
		OnBnClickedBackColor(hWnd);
		break;
	}
}

//控件消息
static void OnCommand(HWND hWnd, UINT id, UINT event)
{
	if(event == BN_CLICKED)
	{
		OnBnClicked(hWnd, id);
	}
	if(event == CBN_SELCHANGE)
	{
		OnDropdownChanged(hWnd, id);
	}
}

static void OnInitDialog(HWND hWnd)
{
	HICON hIcon;
	m_bInit = TRUE;
	hIcon=LoadIcon(m_hInst,MAKEINTRESOURCE(IDI_SERIALTOOL));
	SendMessage(hWnd,WM_SETICON,1,(LPARAM)hIcon);
	SetWindowText(hWnd,"串口调试工具");
	GetClientRect(hWnd,&m_CurRect);
	GetWindowRect(hWnd,&m_MinRect);

	FillSerialPort(GetDlgItem(hWnd,IDC_LIST_PORT));
	FillBaudRate(GetDlgItem(hWnd,IDC_LIST_BAUDRATE));
	FillDataBits(GetDlgItem(hWnd,IDC_LIST_DATABITS));
	FillStopBits(GetDlgItem(hWnd,IDC_LIST_STOPBITS));
	FillParityBit(GetDlgItem(hWnd,IDC_LIST_PARITY));
	SetDlgItemInt(hWnd,IDC_EDIT_INTERVAL,1000,0);
	SendDlgItemMessage(hWnd,IDC_EDIT_RECV,EM_SETLIMITTEXT,INFINITE,0);

	m_hwndSend  = GetDlgItem(hWnd, IDC_EDIT_SEND);
	m_hwndRecv  = GetDlgItem(hWnd, IDC_EDIT_RECV);
	m_textColor = GetSysColor(COLOR_BTNTEXT);
	m_backColor = GetSysColor(COLOR_WINDOW);
	m_backBrush = CreateSolidBrush(m_backColor);
}

static void OnSize(HWND hWnd, WPARAM wParam, int nWidth, int nHeight)
{
	int  dx;
	int  dy;
	dx = nWidth-m_CurRect.right;
	dy = nHeight-m_CurRect.bottom;
	m_CurRect.right = nWidth;
	m_CurRect.bottom= nHeight;

	LockWindowUpdate(hWnd);
	//1.
	MoveDlgItem(hWnd,IDC_EDIT_RECV,dx,dy,MF_RESIZEHEIGHT|MF_RESIZEWIDTH);

	//2.
	MoveDlgItem(hWnd,IDC_EDIT_SEND,dx,dy,MF_RESIZEWIDTH|MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_TEXT_DEVICE,dx,dy,MF_RESIZEWIDTH|MF_OFFSETVER);

	//3.
	MoveDlgItem(hWnd,IDC_TEXT_RECVCOUNT,dx,dy,MF_OFFSETHOR|MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_TEXT_SENDCOUNT,dx,dy,MF_OFFSETHOR|MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_BTN_ZERO,dx,dy,MF_OFFSETHOR|MF_OFFSETVER);

	//4.
	MoveDlgItem(hWnd,IDC_GROUP_TX,dx,dy,MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_BTN_SEND,dx,dy,MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_BTN_SENDFILE,dx,dy,MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_BTN_SENDHEX,dx,dy,MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_BTN_AUTOSEND,dx,dy,MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_BTN_LOADFILE,dx,dy,MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_EDIT_INTERVAL,dx,dy,MF_OFFSETVER);
	MoveDlgItem(hWnd,IDC_TEXT_INTERVAL,dx,dy,MF_OFFSETVER);
	
	LockWindowUpdate(0);
}

static void OnGetMinMaxInfo(HWND hWnd, MINMAXINFO* pMMI)
{
	pMMI->ptMinTrackSize.x = m_MinRect.right - m_MinRect.left;
	pMMI->ptMinTrackSize.y = m_MinRect.bottom - m_MinRect.top;
}

static HBRUSH OnCtlColorEdit(HWND hWnd, HDC hdc, HWND hEdit)
{
	if((hEdit == m_hwndSend) || (hEdit == m_hwndRecv))
	{
		SetTextColor(hdc, m_textColor);
		SetBkColor(hdc, m_backColor);
		return m_backBrush;
	}
	return NULL;
}

INT_PTR WINAPI MainProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	INT_PTR ret;
	switch(msg)
	{
	case WM_INITDIALOG:
		OnInitDialog(hWnd);
		break;
	case WM_HELP:
		PopupAboutBox(m_hInst,hWnd);
		break;
	case WM_SIZE:
		if((wParam == SIZE_RESTORED)||(wParam==SIZE_MAXIMIZED))
		{
			OnSize(hWnd,wParam,LOWORD(lParam),HIWORD(lParam));
		}
		break;
	case WM_CTLCOLORSTATIC:
	case WM_CTLCOLOREDIT:
		ret = (INT_PTR)OnCtlColorEdit(hWnd, (HDC)wParam, (HWND)lParam);
		return ret;
	case WM_COMMAND:
		OnCommand(hWnd,LOWORD(wParam),HIWORD(wParam));
		break;
	case WM_GETMINMAXINFO:
		if(m_bInit)
		{
			OnGetMinMaxInfo(hWnd, (MINMAXINFO*)lParam);
			return 1;
		}
		break;
	case WM_TIMER:
		OnTimer(hWnd,wParam);
		break;
	case MSG_RECVDATA:
		UpdateRecvData(hWnd,(BYTE*)lParam,(DWORD)wParam);
		break;
	}
	return 0;
}

int APIENTRY WinMain(HINSTANCE hInstance,HINSTANCE hPrev, LPTSTR lpLine,int nShow)
{
	INITCOMMONCONTROLSEX icm;
	m_hInst = hInstance;
	icm.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icm.dwICC = ICC_PROGRESS_CLASS;
	InitCommonControlsEx(&icm);
	HookWindowCreate();
	DialogBox(m_hInst,MAKEINTRESOURCE(IDD_MAINBOX),NULL,MainProc);
	UnHookWindowCreate();
	return 0;
}
